<?php
#variables de ssession
session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/0a6512b87a.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>ejemplo MVC PHP</title>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">

                <!--preguntamos si la variable get pagina contiene un valor-->
                <?php if(isset($_GET['pagina'])): ?>

                    <?php if($_GET['pagina']== 'registro'): ?>

                        <li class="nav-item">
                            <a class="nav-link active" href="index.php?pagina=registro">Registro</a>
                        </li>

                    <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?pagina=registro">Registro</a>
                        </li>
                        
                    <?php endif ?>

                    <?php if($_GET['pagina']== 'ingreso'): ?>

                    <li class="nav-item">
                        <a class="nav-link active" href="index.php?pagina=ingreso">Ingreso</a>
                    </li>

                    <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?pagina=ingreso">Ingreso</a>
                    </li>

                    <?php endif ?>

                    <?php if($_GET['pagina']== 'inicio'): ?>

                    <li class="nav-item">
                        <a class="nav-link active" href="index.php?pagina=inicio">Inicio</a>
                    </li>

                    <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?pagina=inicio">Inicio</a>
                    </li>

                    <?php endif ?>

                    <!--validar si existe variable de session para agregar email de usuario-->
                    <?php if(isset($_SESSION['validarIngreso'])): ?>

                        <li class="nav-item">
                            <span class="nav-link">Usuario: <?php echo $_SESSION['nombreUsuario']; ?></span>
                        </li>
                    <?php endif ?>

                    <?php if($_GET['pagina']== 'salir'): ?>

                    <li class="nav-item">
                        <a class="nav-link active" href="index.php?pagina=salir">Salir</a>
                    </li>

                    <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?pagina=salir">Salir</a>
                    </li>

                    <?php endif ?>
                
                <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?pagina=registro">Registro</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?pagina=ingreso">Ingreso</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?pagina=inicio">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php?pagina=salir">Salir</a>
                    </li>
                <?php endif ?>
                
            </ul>
        </div>
    </nav>

    <h1>Plantilla PHP</h1>

    <?php

        # isset -> determina si una variable está definida o es NULL
        if(isset($_GET['pagina'])){

            if($_GET['pagina'] == 'registro' || 
                $_GET['pagina'] == 'inicio' || 
                $_GET['pagina'] == 'ingreso' ||
                $_GET['pagina'] == 'editar' || 
                $_GET['pagina'] == 'salir'){

                include "paginas/".$_GET['pagina'].".php";
            }else{
                include"paginas/error404.php";
            }

        }else{
            #pagina por defecto
            include "paginas/registro.php";
        }
        
    ?>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>