<?php

    if (isset($_GET['id'])){

        $nombreColumna = 'id';
        $valor = $_GET['id'];

        $usuario = ControladorFormulario::ctrSeleccionarUsuarios( $nombreColumna, $valor );

        #var_dump($usuario);
    }

?>

<h2>Editar</h2>
<div class="d-flex m-2">
  <form class="mt-2" method="POST">
      <div class="form-group">

        <input type="text" class="form-control" value="<?php echo $usuario['nombre'] ?>" placeholder="Ingrese nombre" name="nombre" required>
      </div>

      <div class="form-group">

        <input type="email" class="form-control" value="<?php echo $usuario['email'] ?>" placeholder="Ingrese email" name="email" required>
      </div>

      <div class="form-group">
        <input type="password" class="form-control" placeholder="Nueva contraseña" name="password">
        <input type="hidden" name="passwordActual" value="<?php echo $usuario['password'] ?>">
        <input type="hidden" name="idUsuario" value="<?php echo $usuario['id'] ?>">
      </div>

      <?php
        
        $actualizar = new ControladorFormulario();
        $actualizar->ctrActualizarRegistro();
      ?>
      <button type="submit" class="btn btn-primary">Actualizar</button>
  </form>
</div>
  