<h2>Ingreso</h2>
<div class="d-flex m-2">
  <form class="mt-2" method="POST">

      <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" id="email" placeholder="Ingrese email" name="email" required>
      </div>

      <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password" name="password" required>
      </div>

      <?php

        #llamada metodo no estatico
        #los no estaticos se utilizan para que se ejecuten en el controlador
        #los estaticos se utilizan para que se ejecuten en la vista
        $ingreso = new ControladorFormulario();
        $ingreso->ctringreso();
      
      ?>
      <button type="submit" class="btn btn-primary">Ingresar</button>
  </form>
</div>