<h2>Registro</h2>
<div class="d-flex m-2">
  <form class="mt-2" method="POST">
      <div class="form-group">
        <label for="nombre">Nombre:</label>
        <input type="text" class="form-control" id="nombre" placeholder="Ingrese nombre" name="nombre" required>
      </div>

      <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" id="email" placeholder="Ingrese email" name="email" required>
      </div>

      <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password" name="password" required>
      </div>

      <?php
        #llamada a metodo dinamico
        #$registro = new ControladorFormulario();
        #$registro->ctrRegistroUsuario();

        #llamada metodo estatico
        $registroEstatico = ControladorFormulario::ctrRegistroEstatico();

        if($registroEstatico == "ok"){
          echo("<script> 
          
            if(window.history.replaceState){
              window.history.replaceState( null, null, window.location.href );
              console.log('variables post limpiadas');
            }else{
              console.log('no hay valores en las variables post');
            }

          </script>");
          echo('<div class="alert alert-success">Usuario registrado correctamente.</div>');
        }
      ?>
      <button type="submit" class="btn btn-primary">Enviar</button>
  </form>
</div>
  