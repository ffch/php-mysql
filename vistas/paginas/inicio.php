<?php

#validar que exista la variable de session
if(isset($_SESSION['validarIngreso'])){

    #si el valor es distinto de ok redirigir a la pagina ingreso
    if($_SESSION['validarIngreso'] != "ok"){

        echo("<script> 
 
                window.location = 'index.php?pagina=ingreso';

            </script>");

        return;
    }
}else{

    echo("<script> 
 
                window.location = 'index.php?pagina=ingreso';

        </script>");

    return;

}

#traer todos los registros de la bd
$usuarios = ControladorFormulario::ctrSeleccionarUsuarios( null, null);

?>

<table class="table table-hover table-dark mt-4 container">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Email</th>
            <th scope="col">Fecha Creacion</th>
            <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($usuarios as $key => $value): ?>
            <tr>
                <td><?php echo $key+1; ?></td>
                <td><?php echo $value['nombre']; ?></td>
                <td><?php echo $value['email']; ?></td>
                <td><?php echo $value['fecha']; ?></td>
            <td>
                <div class="btn-group">
                    <a href="index.php?pagina=editar&id=<?php echo $value['id']; ?>" class="btn btn-warning mx-2">Editar</a>
                    <form method="post">
                        <input type="hidden" value="<?php echo $value['id']; ?>" name="eliminarRegistro">
                        <button type="submit" class="btn btn-danger">Eliminar</button>

                        <?php

                            $eliminar = new ControladorFormulario();
                            $eliminar -> ctrEliminarRegistro();

                        ?>
                    </form>
                    
                </div>
            </td>
        </tr>
        
        <?php endforeach ?>
        
        
    </tbody>
</table>
