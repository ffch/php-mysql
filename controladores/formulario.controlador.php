<?php

class ControladorFormulario{

    #registro de usuarios 
    public function ctrRegistroUsuario(){

        if(isset($_POST['nombre'])){
            print($_POST['nombre']);
        }else{
            print('no hay nada');
        }
    }

    #registro estatico
    #el metodo estatico se utiliza para la manipulacion de datos
    static public function ctrRegistroEstatico(){

        if(isset($_POST['nombre'])){
            
            $nombreTabla = "registros";
            $datos = ["nombre" => $_POST['nombre'], "email" => $_POST['email'], "password" => $_POST['password']];

            #llamada al metodo del modelo
            $respuesta = ModeloFormulario::mdlRegistrarUsuario($nombreTabla, $datos);

            #respuesta a la capa vista ( "ok" o un error xd) 
            return $respuesta;
        }
        
    }

    static public function ctrSeleccionarUsuarios( $nombreColumna, $valor ){

        $nombreTabla = "registros";

        $respuesta = ModeloFormulario::mdlSeleccionarUsuarios($nombreTabla, $nombreColumna, $valor);

        return $respuesta;
    }

    public function ctrIngreso(){

        #preguntamos si viene una variable en el metodo post con el nombre de email
        if(isset($_POST['email'])){

            $nombreTabla = 'registros';
            $nombreColumna = 'email';
            $valor = $_POST['email'];

            #consulta a la bd
            $respuesta = ModeloFormulario::mdlSeleccionarUsuarios($nombreTabla, $nombreColumna, $valor);
            
            if($respuesta['email'] == $_POST['email'] && $respuesta['password'] == $_POST['password']){

                #inicializar variable de session 
                $_SESSION['validarIngreso'] = "ok";
                $_SESSION['emailUsuario'] = $respuesta['email'];
                $_SESSION['nombreUsuario'] = $respuesta['nombre'];

                echo("<script> 
          
                if(window.history.replaceState){
                    window.history.replaceState( null, null, window.location.href );
                    console.log('variables post limpiadas');
                }else{
                    console.log('no hay valores en las variables post');
                }

                window.location = 'index.php?pagina=inicio';

                </script>");
            }else{
                echo("<script> 
          
                if(window.history.replaceState){
                    window.history.replaceState( null, null, window.location.href );
                    console.log('variables post limpiadas');
                }else{
                    console.log('no hay valores en las variables post');
                }

                </script>");
                echo('<div class="alert alert-danger">Usuario no registrado.</div>');
            }
            #print_r($respuesta);
        }
    }

    public function ctrActualizarRegistro(){

        if(isset($_POST['nombre'])){
            
            if( $_POST['password'] != ""){

                $password = $_POST['password'];
            }else{

                $password = $_POST['passwordActual'];
            }

            $nombreTabla = "registros";
            $datos = ["id" => $_POST['idUsuario'], "nombre" => $_POST['nombre'], "email" => $_POST['email'], "password" => $password];

            #llamada al metodo del modelo
            $respuesta = ModeloFormulario::mdlActualizarUsuario($nombreTabla, $datos);

            if ( $respuesta == 'ok'){

                echo("<script> 
          
                    if(window.history.replaceState){
                    window.history.replaceState( null, null, window.location.href );
                    console.log('variables post limpiadas');
                    }else{
                    console.log('no hay valores en las variables post');
                    }

                </script>");

                echo('<div class="alert alert-success">Usuario actualizado correctamente.</div>');

                echo("<script> 
          
                    setTimeout(()=>{
                        window.location = 'index.php?pagina=inicio';
                    }, 3000);

                </script>");
            }
        }
    }

    public function ctrEliminarRegistro(){

        if(isset($_POST['eliminarRegistro'])){
            
            $nombreTabla = 'registros';
            $nombreColumna = 'id';
            $valor = $_POST['eliminarRegistro'];

            #consulta a la bd
            $respuesta = ModeloFormulario::mdlEliminarRegistro($nombreTabla, $nombreColumna, $valor);

            if($respuesta == 'ok'){

                echo("<script> 
          
                    if(window.history.replaceState){
                        //refresca la pagina actual
                        window.history.replaceState( null, null, window.location.href );
                        
                    }

                    window.location = 'index.php?pagina=inicio';

                </script>");
            }
        }
    }
}