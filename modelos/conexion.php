<?php

class Conexion{

    static public function conectar(){

        #parametros PDO ( "nombre servidor; nombre base de datos", "usuario", "contraseña")
        # la configuracion del charset es opcional 
        $link = new PDO("mysql:host=localhost;dbname=curso-php;charset=utf8", "root", "");

        return $link;
    }
}