<?php

require_once "conexion.php";

class ModeloFormulario{

    static public function mdlRegistrarUsuario($nombreTabla, $datos){

        #la variable stmt hace referencia a -> statement = declaracion
        #en los datos de VALUES, los parametros va con : esto significa que van ocultos
        $stmt = Conexion::conectar()->prepare(
            "INSERT INTO $nombreTabla(nombre, email, password) VALUES ( :nombre, :email, :password)"
        );

        #bindParam( "nombre del parametro a bindear", "variable a bindear", tipo de dato )
        #binding es ligar o sincronizar datos
        $stmt -> bindParam(":nombre", $datos['nombre'], PDO::PARAM_STR);
        $stmt -> bindParam(":email", $datos['email'], PDO::PARAM_STR);
        $stmt -> bindParam(":password", $datos['password'], PDO::PARAM_STR);

        #comprobar si se ejecuta el objeto stmt
        if($stmt->execute()){
            #retorno a la capa controlador
            return "ok";
        }else{
            #imprimir error
            print_r(Conexion::conectar()->errorInfo());
        }

        #cerrar conexion
        $stmt->closeCursor();
        #vaciar objeto stmt
        $stmt = null;
    }

    static public function mdlSeleccionarUsuarios($nombreTabla, $nombreColumna, $valor){

        if( $nombreColumna==null && $valor==null ){

            $stmt = Conexion::conectar()->prepare(
                "select *, date_format(fecha, '%d/%m/%Y') as fecha from $nombreTabla"
            );
    
            if($stmt->execute()){
    
                #fetchAll devuelve un objeto con todos los registros
                return $stmt->fetchAll();
            }else{
                #imprimir error
                print_r(Conexion::conectar()->errorInfo());
            }
        }else{

            $stmt = Conexion::conectar()->prepare(
                "select *, date_format(fecha, '%d/%m/%Y') as fecha from $nombreTabla
                 where $nombreColumna = :$nombreColumna"
            );

            $stmt -> bindParam(":".$nombreColumna, $valor, PDO::PARAM_STR);
    
            if($stmt->execute()){
    
                #fetch devuelve un objeto con solo un registro
                return $stmt->fetch();
            }else{
                #imprimir error
                print_r(Conexion::conectar()->errorInfo());
            }
        }

        #cerrar conexion
        $stmt->closeCursor();
        #vaciar objeto stmt
        $stmt = null;
    }

    static function mdlActualizarUsuario($nombreTabla, $datos){

        $stmt = Conexion::conectar()->prepare(
            "UPDATE $nombreTabla SET nombre=:nombre, email=:email, password=:password WHERE id=:id"
        );

        $stmt -> bindParam(":nombre", $datos['nombre'], PDO::PARAM_STR);
        $stmt -> bindParam(":email", $datos['email'], PDO::PARAM_STR);
        $stmt -> bindParam(":password", $datos['password'], PDO::PARAM_STR);
        $stmt -> bindParam(":id", $datos['id'], PDO::PARAM_INT);

        if($stmt->execute()){
            #retorno a la capa controlador
            return "ok";
        }else{
            #imprimir error
            print_r(Conexion::conectar()->errorInfo());
        }

        #cerrar conexion
        $stmt->closeCursor();
        #vaciar objeto stmt
        $stmt = null;
    }

    static function mdlEliminarRegistro($nombreTabla, $nombreColumna, $valor){

        $stmt = Conexion::conectar()->prepare(
            
            "DELETE FROM $nombreTabla WHERE $nombreColumna=:id"
        );

        $stmt -> bindParam(":id", $valor, PDO::PARAM_INT);

        if($stmt->execute()){
            #retorno a la capa controlador
            return "ok";
        }else{
            #imprimir error
            print_r(Conexion::conectar()->errorInfo());
        }

        #cerrar conexion
        $stmt->closeCursor();
        #vaciar objeto stmt
        $stmt = null;
    }
}