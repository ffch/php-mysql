<?php

#require_once carga el archivo solo una vez
#requerimos el archivo necesario para instanciar la clase Controladorplantilla
require_once "controladores/plantilla.controlador.php";

#en index requerimos los modelos y controladores para ser utilizados en otros ficheros
#solo la conexion es la que se requiere en la capa Modelo
require_once "controladores/formulario.controlador.php";
require_once "modelos/formulario.modelo.php";

//instanciamos
$plantilla = new ControladorPlantilla();
$plantilla->ctlTraerPlantilla();

?>